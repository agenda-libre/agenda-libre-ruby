# The code to manage adl scheduling. Repeat events, is managed here
module Schedule
  def schedule
    IceCube::Schedule.new start_time, end_time: end_time do |s|
      s.add_recurrence_rule prepare_schedule.count repeat + 1
    end
  end

  private

  def prepare_schedule
    r = IceCube::Rule.send rule.split('_').first
    if rule == 'monthly_day'
      r.day_of_week start_time.wday => [((start_time.day - 1) / 7) + 1]
    else
      r
    end
  end
end
