(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      width: '100%',
      height: '40em',
      menubar: false,
      branding: false,
      language: 'fr_FR',
      selector: 'textarea.description',
      content_css: '/assets/application-3f0bb7f3e41c02b3772e8f51bccaa7d4b21e124f775aa2ff998ddec70e42d1ea.css',
      relative_urls: false,
      entity_encoding: 'raw',
      document_base_url: '/',
      paste_data_images: false,
      add_unload_trigger: true,
      browser_spellcheck: true,
      style_formats_autohide: true,
      toolbar: [' cut copy paste | undo redo | link image media charmap table | code visualblocks searchreplace', ' removeformat bold italic strikethrough superscript subscript blockquote forecolor backcolor | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify alignnone'],
      toolbar_sticky: true,
      plugins: 'lists advlist autolink link image charmap preview table fullscreen searchreplace media insertdatetime visualblocks wordcount code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
