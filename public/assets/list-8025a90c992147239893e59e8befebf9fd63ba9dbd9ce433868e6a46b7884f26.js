(function() {
  var visit;

  $(document).on('ajax:success', 'a[rel=next]', function(event, data) {
    var next;
    $(this).parents('tfoot').prev().append($('> tbody > tr', data)).find('td.view a').each(function() {
      return visit($(this));
    });
    next = $('a[rel=next]', data).attr('href');
    if (next != null) {
      return $(this).attr('href', next).parents('tfoot').show();
    } else {
      return $(this).parents('tfoot').hide();
    }
  });

  $(document).on('turbolinks:load', function() {
    return $('table.list td.view a').each(function() {
      return visit($(this));
    });
  });

  visit = (function(_this) {
    return function(elt) {
      return elt.closest('tr').addClass('view').click(function(event) {
        var target;
        target = $(event.target);
        if (!(target.is('a') || target.parent().is('a'))) {
          return Turbolinks.visit(elt.attr('href'));
        }
      });
    };
  })(this);

}).call(this);
