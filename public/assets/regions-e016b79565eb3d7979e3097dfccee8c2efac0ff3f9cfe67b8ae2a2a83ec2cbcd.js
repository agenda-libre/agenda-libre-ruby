(function() {
  $(document).on('turbolinks:load', function() {
    return $('.region :input').click(function(e) {
      return e.originalEvent.x && !e.target.id.endsWith('_') && e.target.blur();
    });
  });

}).call(this);
