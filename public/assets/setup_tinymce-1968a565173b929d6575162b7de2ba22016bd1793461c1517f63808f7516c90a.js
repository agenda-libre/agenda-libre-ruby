(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      width: '100%',
      height: '40em',
      menubar: false,
      branding: false,
      language: 'fr_FR',
      selector: 'textarea.description',
      license_key: 'gpl',
      content_css: '/assets/application-f3f90845334e37141d09e1257074b62517b520713c3170b5cb2d3531b832842c.css',
      relative_urls: false,
      entity_encoding: 'raw',
      document_base_url: '/',
      paste_data_images: false,
      add_unload_trigger: true,
      browser_spellcheck: true,
      style_formats_autohide: true,
      toolbar: [' cut copy paste | undo redo | link image media charmap table | code visualblocks searchreplace', ' removeformat bold italic strikethrough superscript subscript blockquote forecolor backcolor | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify alignnone'],
      toolbar_sticky: true,
      plugins: 'lists advlist autolink link image charmap preview table fullscreen searchreplace media insertdatetime visualblocks wordcount code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
