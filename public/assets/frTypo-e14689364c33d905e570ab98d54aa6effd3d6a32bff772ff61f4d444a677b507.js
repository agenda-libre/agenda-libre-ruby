
/*
  @licstart  The following is the entire license notice for the JavaScript code in this page.

  frTypo, la typographie française simplifiée

      Copyright (C) 2013  acoeuro

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  @licend  The above is the entire license notice for the JavaScript code in this page.
 */

(function() {
  var regexp, regexpPost;

  regexp = /(^|[\wàéèêç])\s*([!?:;»%€¢]+)(\s|[^\w\/]|$)/g;

  regexpPost = /([«])\s*([\w])/g;

  $(document).on('turbolinks:load', function() {
    if ($('html').attr('lang') === 'fr') {
      return $('body *').contents().filter(function() {
        return this.nodeType === Node.TEXT_NODE;
      }).filter(function() {
        return 0 > ['CODE', 'PRE', 'STYLE', 'TEXTAREA'].indexOf(this.parentNode.tagName);
      }).filter(function() {
        return !$(this).parent().hasClass('finePre') && !$(this).parent().hasClass('start_time') && !$(this).parent().hasClass('end_time');
      }).filter(function() {
        return (this.nodeValue.match(regexp) != null) || (this.nodeValue.match(regexpPost) != null);
      }).each(function() {
        return $(this).replaceWith(function() {
          return this.nodeValue.replace(regexp, '$1<span class="finePre">$2</span>$3').replace(regexpPost, '<span class="finePost">$1</span>$2');
        });
      });
    }
  });

}).call(this);
