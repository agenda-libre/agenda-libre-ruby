(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      width: '100%',
      height: '40em',
      menubar: false,
      branding: false,
      language: 'fr_FR',
      selector: 'textarea.description',
      license_key: 'gpl',
      content_css: '/assets/application-88f2d67be282d000a22abae5798d5ddffaebdc4cca623b9923c2d849fe7364de.css',
      relative_urls: false,
      entity_encoding: 'raw',
      document_base_url: '/',
      paste_data_images: false,
      add_unload_trigger: true,
      browser_spellcheck: true,
      style_formats_autohide: true,
      toolbar: [' cut copy paste | undo redo | link image media charmap table | code visualblocks searchreplace', ' removeformat bold italic strikethrough superscript subscript blockquote forecolor backcolor | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify alignnone'],
      toolbar_sticky: true,
      plugins: 'lists advlist autolink link image charmap preview table fullscreen searchreplace media insertdatetime visualblocks wordcount code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
