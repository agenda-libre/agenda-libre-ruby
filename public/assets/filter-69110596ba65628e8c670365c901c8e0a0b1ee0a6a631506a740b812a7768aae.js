(function() {
  $(document).on('turbolinks:load', function() {
    $('body.pages form :input').prop('disabled', false);
    $('form').submit(function() {
      $('input[name=utf8]').prop('disabled', true);
      return $('button').prop('disabled', true);
    });
    return $('.region :input').click(function(e) {
      return e.originalEvent.x && !e.target.id.endsWith('_') && e.target.blur();
    });
  });

}).call(this);
