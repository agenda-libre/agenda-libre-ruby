(function() {
  $(document).on('turbolinks:load', function() {
    $('table.list.dates tbody tr').each(function() {
      var vals;
      vals = $(this).find('td.quantity').map(function() {
        var val;
        val = $(this).find('a').html().replace(' ', '').trim();
        if (val && val !== '') {
          return parseInt(val);
        } else {
          return 0;
        }
      });
      return $(this).find('.sparkline').sparkline(vals, {
        width: '5em'
      });
    });
    return $('table.list.dates tfoot').each(function() {
      var vals;
      vals = $(this).find('th.quantity').map(function() {
        return parseInt($(this).html().replace(' ', ''));
      });
      return $(this).find('.sparkline').sparkline(vals, {
        type: 'bar',
        height: '3em',
        barWidth: '100%',
        barColor: '#9CC5EE',
        barSpacing: 2
      });
    });
  });

}).call(this);
