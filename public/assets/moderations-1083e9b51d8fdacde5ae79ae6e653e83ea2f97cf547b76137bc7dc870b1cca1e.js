(function() {
  $(document).on('turbolinks:load', function() {
    $('body.moderations .radios label').click(function() {
      return $('body.moderations #event_reason').parent().slideUp();
    });
    return $('body.moderations .radios label:last-child').click(function() {
      return $('body.moderations #event_reason').parent().slideDown();
    });
  });

}).call(this);
