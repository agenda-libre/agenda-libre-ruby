# Helping methods to display actualities
module ActusHelper
  TAGS_TO_SANITIZE = %w[p h1 h2 h3 h4 br table tr th td ul ol li a strong b em i img sub sup span].freeze
  ATTS_TO_SANITIZE = %w[href src width height style title name id].freeze

  def display_actu_description(actu)
    # Ensure images are absolute
    description = actu.description
                      .gsub(%r{src="//}, 'src="https://"')
                      .gsub(%r{src="/}, "src=\"#{actu.orga.url}/")
                      .gsub(%r{src="./}, "src=\"#{actu.orga.url}/")
                      .gsub(/src="((?!http))/, "src=\"#{actu.orga.url}/")

    sanitize description, tags: TAGS_TO_SANITIZE, attributes: ATTS_TO_SANITIZE
  end
end
