$(document).on 'turbolinks:load', ->
  $('#event_start_time').change ->
    if $('#event_start_time').val() >= $('#event_end_time').val()
      $('#event_end_time').val($('#event_start_time').val())
  $('#event_end_time').change ->
    if !$('#event_start_time').val()
      $('#event_start_time').val($('#event_end_time').val())

  if !Modernizr.inputtypes['datetime-local'] && Modernizr.inputtypes.date && Modernizr.inputtypes.time
    # Small hack to manage datetime-local for older browsers
    $('[type=datetime-local]').hide()
      .after ->
        "<input type='time' required value='#{this.value && new Date(this.value).toTimeString().split(' ')[0]}'></input>"
      .after ->
        "<input type='date' required value='#{this.value && new Date(this.value).toISOString().split('T')[0]}'></input>"

    $('[type=date], [type=time]').css('flex-grow', 0).css('margin', 0)
      .change ->
        date = $(this).parent().find('[type=date]')
        time = $(this).parent().find('[type=time]')
        return if !date.val() || !time.val()
        target = $(this).parent().find('[type=datetime-local]')
        target.val date.val() + 'T' + time.val()

  # Quick mechanism so that the ice cube rule only appears when useful
  $('#event_repeat').each ->
    if $(this).val() == '0'
      $('.field.rule').hide()

    $(this).change ->
      if $(this).val() > 0
        $('.field.rule').show()
        $('.field.rule input').attr 'required', 'required'
      else
        $('.field.rule').hide()
        $('.field.rule input').removeAttr 'required'

  # Manage event tags edition
  $('#event_tags').each ->
    elt = $(this)
    $.ajax
      url: '/tags.json'
    .done (data) ->
      tags = jQuery.map data, (n) -> n[0]
