# Pagination link
$(document).on 'ajax:success', 'a[rel=next]', (event, data) ->
	$(this).parents('tfoot')
		.prev()
		.append($('> tbody > tr', data))
		.find('td.view a').each ->
			visit $(this)

	next = $('a[rel=next]', data).attr 'href'
	if next?
		$(this).attr('href', next)
			.parents('tfoot').show()
	else
		$(this).parents('tfoot').hide()

# Click on a table.list row
$(document).on 'turbolinks:load', ->
	$('table.list td.view a').each -> visit $(this)

visit = (elt) =>
	elt.closest('tr').addClass('view').click (event) ->
		target = $(event.target)
		unless target.is('a') || target.parent().is('a')
			Turbolinks.visit elt.attr 'href'
