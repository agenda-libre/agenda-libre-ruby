json.set! '@context', 'http://schema.org'
json.set! '@type', 'ProfilePage'
json.dateCreated @orga.created_at.iso8601
json.dateModified @orga.updated_at.iso8601

json.mainEntity do
  json.set! '@type', 'Organization'
  json.id @orga.id.to_s
  json.name @orga.name

  json.location do
    json.set! '@type', 'Place'
    json.name @orga.place_name if @orga.place_name

    json.address do
      json.set! '@type', 'PostalAddress'
      json.streetAddress @orga.address if @orga.address
      json.city @orga.city if @orga.city
      json.addressRegion @orga.region.try(:name)
    end
  end

  json.description sanitize @orga.description
  json.image(Nokogiri::HTML.fragment(@orga.description)
                          .css('img')
                          .map(&:attributes)
                          .map { |a| a['src'].value })
  json.url @orga.url if @orga.url
  json.email @orga.contact if @orga.contact
  json.keywords @orga.tag_list
end
