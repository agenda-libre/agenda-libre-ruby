json.array!(@orgas) do |orga|
  json.extract! orga, :id, :name, :description,
                :place_name, :address, :city, :url, :contact,
                :region_id, :latitude, :longitude
  json.region orga.region.try(:name)
  json.url orga_url(orga, format: :json)
  json.tags orga.tags.join ' '
end
