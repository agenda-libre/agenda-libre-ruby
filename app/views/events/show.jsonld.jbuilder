json.set! '@context', 'http://schema.org'
json.set! '@type', 'Event'
json.eventStatus 'http://schema.org/EventScheduled'

json.id @event.id.to_s
json.name @event.title
json.startDate @event.start_time.iso8601
json.endDate @event.end_time.iso8601

json.location do
  json.set! '@type', 'Place'
  json.name @event.place_name if @event.place_name

  json.address do
    json.set! '@type', 'PostalAddress'
    json.streetAddress @event.address if @event.address
    json.city @event.city if @event.city
    json.addressRegion @event.region.try(:name)
  end
end

json.description sanitize @event.description
json.image(Nokogiri::HTML.fragment(@event.description)
                         .css('img')
                         .map(&:attributes)
                         .map { |a| a['src'].value })
json.url @event.url if @event.url
json.keywords @event.tag_list
