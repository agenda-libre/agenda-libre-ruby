ActiveAdmin.register User do
  index do
    column :login
    column :email
    column :firstname
    column :lastname
    column :current_sign_in_at
    column :last_sign_in_at
    column :sign_in_count
    actions
  end

  filter :login
  filter :email
  filter :firstname
  filter :lastname

  form do |f|
    f.inputs 'Admin Details' do
      f.input :login
      f.input :email
      f.input :firstname
      f.input :lastname
    end
    f.actions
  end

  sidebar :versions, only: :show do
    link_to PaperTrail::Version.where(whodunnit: resource.id).count,
            "../versions?q[whodunnit_eq]=#{resource.id}"
  end

  controller do
    def permitted_params
      params.permit user: %i[login email firstname lastname]
    end
  end

  before_create do |user|
    user.password = Devise.friendly_token.first(8)
  end

  after_create do |user|
    logger.info 'Sending initialisation mail to moderator'
    user.send_reset_password_instructions
  end
end
