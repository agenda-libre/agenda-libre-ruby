ActiveAdmin.register Actu do
  permit_params :guid, :title, :link, :description, :published_at

  scope :all
  scope :active, default: true

  config.sort_order = 'published_at_desc'

  index do
    column :orga
    column :title do |actu|
      link_to actu.title, actu.link
    end
    column :published_at
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :orga
      row :title
      row :guid
      row :link do
        link_to actu.link, actu.link
      end
      row :description do
        sanitize actu.description
      end
      row :published_at
      row :created_at
    end
    active_admin_comments
  end
end
