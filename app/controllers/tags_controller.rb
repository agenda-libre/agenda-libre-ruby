# Manage event and organisation tags
class TagsController < InheritedResources::Base
  before_action :redirect_to_tag, only: [:index], if: -> { params[:tag].present? && params[:term].nil? }
  before_action :set_orgas, if: -> { params[:id].present? }

  def index
    @tags = ActsAsTaggableOn::Tag.where('taggings_count > ?',
                                        Rails.configuration.cloud_threshold)
    if params[:term].present?
      # Used to autocomplete tags
      @tags = @tags.select(:id, :name, 'name AS label')
                   .where('name LIKE ?', "#{params[:term]}%")
    end

    respond_to do |format|
      format.html
      format.json { render json: @tags }
    end
  end

  def show
    @events = Event.tag(params[:id]).reorder('events.start_time').moderated.includes(:tags)
    @futures = @events.future
    @pasts = @events.past.reverse

    respond_to do |format|
      format.html
      format.json { render json: @events }
    end
  end

  private

  # Splits, groups, rejects the less used
  def set_orgas
    @orgas = (apply_scopes(Orga.moderated.active.tag(params[:id])) +
              Orga.moderated.active
                 .where(
                   'lower(orgas.name) like lower(?)',
                   "%#{params[:id].tr '-', '%'}%"
                 )
             ).uniq
  end

  # So that users can come from the search/filter page
  def redirect_to_tag
    redirect_to tag_path params[:tag]
  end
end
