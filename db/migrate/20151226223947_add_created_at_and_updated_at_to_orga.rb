# Adding created_at and updated_at to orgas
class AddCreatedAtAndUpdatedAtToOrga < ActiveRecord::Migration
  def change
    add_column :orgas, :created_at, :datetime
    add_column :orgas, :updated_at, :datetime

    reversible do |dir|
      dir.up do
        # add a CHECK constraint
        execute <<-SQL.squish
          UPDATE orgas
          SET created_at = submission_time, updated_at = decision_time;
        SQL
      end
    end
  end
end
