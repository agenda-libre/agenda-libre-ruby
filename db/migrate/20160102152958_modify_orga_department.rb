# Change department to be a string
class ModifyOrgaDepartment < ActiveRecord::Migration
  def change
    change_column :orgas, :department, :string
  end
end
