update adl_be.orgas
  set tags = (
		  select group_concat(name separator ' ')
			  from adl_be.taggings
				join adl_be.tags on taggings.tag_id=tags.id
			 where taggable_type = 'Orga'
			   and taggable_id = orgas.id)
 where tags is null;

update adl_be.events
  set tags = (
		  select group_concat(name separator ' ')
			  from adl_be.taggings
				join adl_be.tags on taggings.tag_id=tags.id
			 where taggable_type = 'Event'
			   and taggable_id = events.id)
 where tags is null;

update adl_ch.orgas
  set tags = (
		  select group_concat(name separator ' ')
			  from adl_ch.taggings
				join adl_ch.tags on taggings.tag_id=tags.id
			 where taggable_type = 'Orga'
			   and taggable_id = orgas.id)
 where tags is null;

update adl_ch.events
  set tags = (
		  select group_concat(name separator ' ')
			  from adl_ch.taggings
				join adl_ch.tags on taggings.tag_id=tags.id
			 where taggable_type = 'Event'
			   and taggable_id = events.id)
 where tags is null;

update adl_fr.regions set url = null where id = 32;
insert into adl_fr.regions (id, name, region_id)
select id + 38, name, 32
  from adl_be.regions
 where region_id = 28
 order by id;

update adl_fr.regions set url = null where id = 36;
insert into adl_fr.regions (id, name, region_id)
select id + 70, name, 36
  from adl_ch.regions
 where region_id = 58
 order by id;


insert into orgas (region_id, department, name, url, city, kind_id, feed, contact, submitter, moderated, submission_time, decision_time, secret, deleted, active, description, created_at, updated_at, tags, diaspora, place_name, address, latitude, longitude)
select region_id + 38, department, name, url, city, kind_id, feed, contact, submitter, moderated, submission_time, decision_time, secret, deleted, active, description, created_at, updated_at, tags, diaspora, place_name, address, latitude, longitude
  from adl_be.orgas
 order by id;

insert into orgas (region_id, department, name, url, city, kind_id, feed, contact, submitter, moderated, submission_time, decision_time, secret, deleted, active, description, created_at, updated_at, tags, diaspora, place_name, address, latitude, longitude)
select region_id + 70, department, name, url, city, kind_id, feed, contact, submitter, moderated, submission_time, decision_time, secret, deleted, active, description, created_at, updated_at, tags, diaspora, place_name, address, latitude, longitude
  from adl_ch.orgas
 order by id;


insert into events (
	region_id,
	title,
	description,
	start_time,
	end_time,
	city,
	locality,
	url,
	contact,
	submitter,
	moderated,
	tags,
	secret,
	decision_time,
	submission_time,
	moderator_mail_id,
	submitter_mail_id,
	address,
	latitude,
	longitude,
	lock_version,
	place_name,
	`repeat`,
	rule
)
select region_id + 38,
	title,
	description,
	start_time,
	end_time,
	city,
	locality,
	url,
	contact,
	submitter,
	moderated,
	tags,
	secret,
	decision_time,
	submission_time,
	moderator_mail_id,
	submitter_mail_id,
	address,
	latitude,
	longitude,
	lock_version,
	place_name,
	`repeat`,
	rule
  from adl_be.events
 order by id;

insert into events (
	region_id,
	title,
	description,
	start_time,
	end_time,
	city,
	locality,
	url,
	contact,
	submitter,
	moderated,
	tags,
	secret,
	decision_time,
	submission_time,
	moderator_mail_id,
	submitter_mail_id,
	address,
	latitude,
	longitude,
	lock_version,
	place_name,
	`repeat`,
	rule
)
select region_id + 70,
	title,
	description,
	start_time,
	end_time,
	city,
	locality,
	url,
	contact,
	submitter,
	moderated,
	tags,
	secret,
	decision_time,
	submission_time,
	moderator_mail_id,
	submitter_mail_id,
	address,
	latitude,
	longitude,
	lock_version,
	place_name,
	`repeat`,
	rule
  from adl_ch.events
 order by id;
