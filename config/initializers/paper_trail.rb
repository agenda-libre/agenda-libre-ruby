module PaperTrail
  # Extension to add some ransack info
  class Version
    def self.ransackable_associations(_auth_object = nil)
      %w[]
    end

    def self.ransackable_attributes(_auth_object = nil)
      %w[created_at event id item_id item_type object object_changes whodunnit]
    end

    def user
      User.where(id: whodunnit.to_i).first
    end
  end
end
