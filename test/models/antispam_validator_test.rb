require 'test_helper'

# Attempt to fight against some spam
class AntispamTest < ActiveSupport::TestCase
  test 'Title blacklisted' do
    event = Event.new(
      title: 'Just want to say Hi.',
      start_time: Time.zone.now, end_time: 1.hour.from_now,
      description: 'et hop!',
      region: Region.first,
      submitter: 'test@exemple.com'
    )

    assert_not event.valid?, 'Title blacklisted'
  end

  test 'Event submitter blacklisted' do
    event = Event.new(
      title: 'Do not accept',
      start_time: Time.zone.now,
      end_time: 1.hour.from_now,
      description: 'et hop!',
      region: Region.first,
      submitter: 'truc@gmail.com'
    )

    assert_not event.valid?, 'Event submitter blacklisted'
  end

  test 'Orga submitter blacklisted' do
    orga = Orga.new(
      name: 'Do not accept',
      description: 'et hop!',
      region: Region.first,
      submitter: 'truc@gmail.com'
    )

    assert_not orga.valid?, 'Orga submitter blacklisted'
  end

  test 'Only one numerical tag' do
    event = Event.new(
      title: 'Not enough tags',
      start_time: Time.zone.now,
      end_time: 1.hour.from_now,
      description: 'et hop!',
      region: Region.first,
      submitter: 'test_again_again@exemple.com',
      tag_list: '123'
    )

    assert_not event.valid?, 'Only one numerical tag'
  end
end
