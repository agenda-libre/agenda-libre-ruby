require 'test_helper'

# Test event callbacks
class EventCallbacksTest < ActiveSupport::TestCase
  setup do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
  end

  test 'schedule' do
    event = Event.new(
      title: 'hello world',
      start_time: Time.zone.now, end_time: 1.hour.from_now,
      description: 'et hop!',
      city: City.first, region: Region.first,
      url: 'http://example.com',
      submitter: 'contact@example.com',
      tag_list: 'hello world'
    )
    assert_difference 'Event.count' do
      event.save
    end
    assert_not event.moderated?
  end

  test 'moderation' do
    event = Event.new(
      title: 'hello world',
      start_time: 1.hour.from_now, end_time: 2.hours.from_now,
      repeat: 1, rule: 'monthly',
      description: 'et hop!',
      city: City.first, region: Region.first,
      url: 'http://example.com',
      submitter: 'contact@example.com',
      tag_list: 'hello world'
    )

    assert_difference 'Event.count' do
      event.update moderated: 1
    end

    assert_predicate event, :moderated?, event.errors.messages
  end
end
