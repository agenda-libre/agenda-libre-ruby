require 'test_helper'

class NoteTest < ActiveSupport::TestCase
  test 'is invalid without contents' do
    note = Note.new(contents: nil)

    assert_not note.valid?
    assert_not_nil note.errors[:contents]
  end

  test 'is invalid with empty contents' do
    note = Note.new(contents: '')

    assert_not note.valid?
    assert_not_nil note.errors[:contents]
  end
end
