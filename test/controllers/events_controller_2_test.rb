require 'test_helper'

# Event life cycle
class EventsController2Test < ActionDispatch::IntegrationTest
  test 'should get index' do
    get events_url

    assert_response :success
    assert_not_nil assigns :events
  end

  test 'should get index as ics' do
    get events_url format: :ics

    assert_response :success
    assert_not_nil assigns :events

    assert_match(/TZID=/, response.body)
  end

  test 'correct encoding for ics' do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'

    event = Event.last
    event.title = 'L\'Encodé <here>'
    event.address = 'L\'autre encodé'
    event.save!
    get events_url format: :ics

    assert_response :success
    assert_match(/SUMMARY:L'Encodé <here>/, response.body)
    assert_match(/L'autre encodé/, response.body)
  end
end
